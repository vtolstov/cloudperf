package main

import (
	"fmt"
	html "github.com/dersebi/golang_exp/exp/html"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
	"time"
)

import zmq "github.com/alecthomas/gozmq"

var host = ""

func main() {
	context, _ := zmq.NewContext()
	defer context.Close()
	socket, _ := context.NewSocket(zmq.SUB)
	defer socket.Close()

	socket.Connect("tcp://54.247.16.45:50000")
	socket.SetSockOptString(zmq.SUBSCRIBE, "")

	for {
		msg, _ := socket.Recv(0)
		host = string(msg)
		println("Recv", host)

		links = append(links, host)
		res, err := http.Get(host)
		if err != nil {
			panic(err)
		}
		defer res.Body.Close()
		z := html.NewTokenizer(res.Body)
		for {
			tt := z.Next()
			switch tt {
			case html.ErrorToken:
				goto work
				break
			case html.StartTagToken:
				tn, more := z.TagName()
				if !more {
					continue
				}
				switch string(tn) {
				case "img", "link", "script":
					for more {
						var (
							attrName, attrVal []byte
						)
						attrName, attrVal, more = z.TagAttr()
						if string(attrName) == "src" || string(attrName) == "href" {
							emitSrc(attrVal)
						}
					}
					break
				}
				break
			}
		}
	work:
	inWork := worker()
	socket.Send([]byte(inWork), 0)
	}
}

var links []string
var sem = make(chan int, 6)
var wg sync.WaitGroup

func emitSrc(b []byte) {
	link := string(b)
	u, err := url.Parse(link)
	if err != nil {
		panic(err)
	}
	if u.Scheme == "" {
		u.Scheme = "http"
		u, _ := url.Parse(host)
		u.Path = link
		link = u.String()
	}
	links = append(links, link)
}

func worker() string {
	sTime := time.Now()
	for i, link := range links {
		wg.Add(1)
		if i == 0 {
			fetch(link)
		} else {
			go fetch(link)
		}
	}
	wg.Wait()
	eTime := time.Now()
	inWork := eTime.Sub(sTime)
	return inWork.String()
}

func fetch(link string) {
	sem <- 1
	fmt.Printf("fetch: %s\n", link)
	res, err := http.Get(link)
	defer res.Body.Close()
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(ioutil.Discard, res.Body)
	if err != nil {
		panic(err)
	}
	<-sem
	wg.Done()
}
