package main

import (
	"fmt"
)

import zmq "github.com/alecthomas/gozmq"

func main() {
	context, _ := zmq.NewContext()
	defer context.Close()
	cli, _ := context.NewSocket(zmq.REP)
	defer cli.Close()
	cli.Bind("tcp://0.0.0.0:55000")

	socket, _ := context.NewSocket(zmq.PUB)
	defer socket.Close()
	socket.Bind("tcp://*:50000")

	for {
		msg, _ := cli.Recv(0)
		fmt.Printf("Job: %s\n", msg)
		socket.Send(msg, 0)
		fmt.Printf("Master->Slave: %s\n", msg)
		msg, _ = socket.Recv(0)
		fmt.Printf("Slave->Master: %s\n", msg)
		cli.Send(msg, 0)
	}
}
