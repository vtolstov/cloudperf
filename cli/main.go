package main

import zmq "github.com/alecthomas/gozmq"

var msg = "http://biovitrum.ru"

func main() {
	context, _ := zmq.NewContext()

	socket, _ := context.NewSocket(zmq.REQ)
	socket.Connect("tcp://127.0.0.1:55000")
	socket.Send([]byte(msg), 0)
	println("Send", msg)
	msg, _ := socket.Recv(0)
	fmt.Printf("inWork: %s\n", msg)

}
